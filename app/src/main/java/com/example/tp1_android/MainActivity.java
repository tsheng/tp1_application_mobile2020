package com.example.tp1_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static String[] items =null;
     RecyclerView.LayoutManager layoutManager;
    AnimalList animalList = new AnimalList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items = AnimalList.getNameArray();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.animalRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(new IconicAdapter());

        /*

        final ListView animal = findViewById(R.id.liste_animal);
        AnimalList animalList = new AnimalList();

        String [] nom = animalList.getNameArray();

        ArrayAdapter <String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 , nom);
        animal.setAdapter(arrayAdapter);

        animal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                String monNomAnimal = animal.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, "You selected:" + monNomAnimal,
                        Toast.LENGTH_LONG).show();
                intent.putExtra("animalName",monNomAnimal);
                startActivity(intent);

            }
        });*/


    }

    class IconicAdapter extends RecyclerView.Adapter<IconTextExemple> {
        @Override
        public IconTextExemple onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new IconTextExemple(getLayoutInflater().inflate(R.layout.activity_icon_text, parent, false)));
        }

        @Override
        public void onBindViewHolder(IconTextExemple iconTextExemple, int position) {
            iconTextExemple.bindModel( AnimalList.getNameArray()[position]);
        }


        @Override
        public int getItemCount() {
            return (AnimalList.getNameArray().length);
        }
    }
}


