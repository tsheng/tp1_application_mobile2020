package com.example.tp1_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    private Animal animal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        Intent intent = getIntent();
        if(intent!=null){


            //initialise une string qui prend en argument le nom dans activity_main
            String nom = getIntent().getStringExtra("animalName");
            //puis, avec methode getName, il va mettre comme nom d'animal l'argument name ici
            animal = AnimalList.getName(nom);

            TextView monNom = (TextView) findViewById(R.id.nomAnimal);
            monNom.setText(nom);


            //Crée une ImageView dont l'argument est récupéré dans le fichier activity_main
            ImageView image = findViewById(R.id.imageAnimal);
            int id = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
            image.setImageResource(id);

            TextView esperance = (TextView) findViewById(R.id.esperanceVie);
            esperance.setText(animal.getStrHightestLifespan());


            TextView gestation = (TextView) findViewById(R.id.gestation);
            gestation.setText(animal.getStrGestationPeriod());

            TextView poidsNaissance = findViewById(R.id.poidsNaissance);
            poidsNaissance.setText(animal.getStrBirthWeight());

            TextView poidsAdulte = findViewById(R.id.poidsAdulte);
            poidsAdulte.setText(animal.getStrAdultWeight());

            final EditText inputConservation = findViewById(R.id.conservation);
            inputConservation.setText(animal.getConservationStatus() + "");
            Button save = findViewById(R.id.save);

            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //simple utlisation des methodes défini dans la class country pour réécrire dans le champ voulu
                    animal.setConservationStatus(inputConservation.getText().toString());
                    Toast.makeText(getApplication(), "Save !", Toast.LENGTH_LONG).show();
                    finish();
                }
            });

        }
    }
}
