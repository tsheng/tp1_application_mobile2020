package com.example.tp1_android;

import android.content.Intent;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

public class IconTextExemple extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView animalName=null;
    ImageView icon = null;

    IconTextExemple(View v) {
        super(v);
        v.setOnClickListener(this);
        //recup id textview
        animalName=(TextView)v.findViewById(R.id.textView);
        //recup id image
        icon=(ImageView)v.findViewById(R.id.label);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), AnimalActivity.class);
        String monNomAnimal = animalName.getText().toString();
        Toast.makeText(v.getContext(), "You selected:" + monNomAnimal,
                Toast.LENGTH_LONG).show();
        intent.putExtra("animalName",monNomAnimal);
        v.getContext().startActivity(intent);
    }

    public void bindModel(String nameAnimal) {
        animalName.setText(nameAnimal);
/*
        Animal animal = AnimalList.getName(nameAnimal);
        int id = this.itemView.getResources().getIdentifier(animal.getImgFile(), "drawable", this.itemView.getContext().getPackageName());
        Log.d("toto", this.itemView.getContext().getPackageName());
        Log.d("toto", String.valueOf(id));
        icon.setImageResource(id);
*/
    }
}
